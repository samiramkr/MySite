$(document).ready(function () {
    $('#menu_firstPart').click(function (event) {
        event.preventDefault();
        animatedScrollTo('#first');

    });
    $('#menu').draggable();
    $('#menu_mobilePart').click(function (event) {
        event.preventDefault();
        animatedScrollTo('#mobilePart');

    });

    $('#menu_galaxyPart').click(function (event) {
        event.preventDefault();
        animatedScrollTo('#galaxyOfInfo');
    });

    $('#menu_sheltPart').click(function (event) {
        event.preventDefault();
        animatedScrollTo('#shelfIt');
    });
});


function animatedScrollTo(id) {
    $(window).bind("scroll", function () {
        var page = $(this).scrollTop() / $(window).height();
        if(page > 0.5 && page <=1)
        {
            $("#MobilePart_text").fadeIn(1000);
            $("#GalaxyPart_text").hide();
            $("#ShelfPart_text").hide();

            $("#inside_mobile_mobilePart").fadeIn(1000);
            $("#inside_mobile_galaxy").hide(300);
            $("#inside_mobile_shelf").hide(300);
        }
        if(page>1.5 && page <= 2 )
        {
            $("#GalaxyPart_text").fadeIn(1000);
            $("#MobilePart_text").hide();
            $("#ShelfPart_text").hide();

            $("#inside_mobile_galaxy").fadeIn(1000);
            $("#inside_mobile_mobilePart").hide(300);
            $("#inside_mobile_shelf").hide(300);
        }
        if(page > 2.5 && page <= 3)
        {
            $("#ShelfPart_text").fadeIn(1000);
            $("#MobilePart_text").hide();
            $("#GalaxyPart_text").hide();

            $("#inside_mobile_shelf").fadeIn(1000);
            $("#inside_mobile_mobilePart").hide(300);
            $("#inside_mobile_galaxy").hide(300);
        }
    });
    $('html, body').stop().animate({
        scrollTop: $(id).offset().top
    }, 1500);
}

$(window).bind("scroll", function () {
    var page = $(this).scrollTop() / $(window).height();
    if (page > 0.5 && page <= 1) {
        $("#MobilePart_text").fadeIn(1000);
        $("#GalaxyPart_text").hide();
        $("#ShelfPart_text").hide();

        $("#inside_mobile_mobilePart").fadeIn(1000);
        $("#inside_mobile_galaxy").hide(300);
        $("#inside_mobile_shelf").hide(300);
    }
    if (page > 1.5 && page <= 2) {
        $("#GalaxyPart_text").fadeIn(1000);
        $("#MobilePart_text").hide();
        $("#ShelfPart_text").hide();

        $("#inside_mobile_galaxy").fadeIn(1000);
        $("#inside_mobile_mobilePart").hide(300);
        $("#inside_mobile_shelf").hide(300);
    }
    if (page > 2.5 && page <= 3) {
        $("#ShelfPart_text").fadeIn(1000);
        $("#MobilePart_text").hide();
        $("#GalaxyPart_text").hide();

        $("#inside_mobile_shelf").fadeIn(1000);
        $("#inside_mobile_mobilePart").hide(300);
        $("#inside_mobile_galaxy").hide(300);
    }
    if ($(this).scrollTop() > 550) {
        $("#mobilePart_Pic").fadeIn(1000);
    } else {
        $("#mobilePart_Pic").hide(300);
    }
});


